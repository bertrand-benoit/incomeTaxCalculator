#!/bin/bash
#
# Author: Bertrand Benoit <mailto:contact@bertrand-benoit.net>
# Version: 2.0.0
# Description: French income tax calculator.
#
# usage: see usage function

#####################################################
#                General configuration.
#####################################################

export BSC_CATEGORY="incomeTaxCalculator"

currentDir=$( dirname "$( command -v "$0" )" )
export BSC_GLOBAL_CONFIG_FILE="$currentDir/default.conf"
export BSC_CONFIG_FILE="${HOME:-/home/$( whoami )}/.config/incomeTaxCalculator.conf"

scriptsCommonUtilities="$currentDir/scripts-common/utilities.sh"
[ ! -f "$scriptsCommonUtilities" ] && echo -e "ERROR: scripts-common utilities not found, you must initialize your git submodule once after you cloned the repository:\ngit submodule init\ngit submodule update" >&2 && exit 1
# shellcheck disable=1090
. "$scriptsCommonUtilities"

# Ensures third-party tools are installed.
checkBin bc || errorMessage "This tool requires bc. Install it please, and then run this tool again."

# Reads configuration.
checkAndSetConfig "paths.year.configuration.dir" "$BSC_CONFIG_TYPE_PATH" "$currentDir"
conf_files_dir="$BSC_LAST_READ_CONFIG"

checkAndSetConfig "TAX_REDUCE_INCOME_PERCENT" "$BSC_CONFIG_TYPE_OPTION"
TAX_REDUCE_INCOME_PERCENT="$BSC_LAST_READ_CONFIG"

checkAndSetConfig "TAX_REDUCE_SERVICE_PERCENT" "$BSC_CONFIG_TYPE_OPTION"
TAX_REDUCE_SERVICE_PERCENT="$BSC_LAST_READ_CONFIG"

checkAndSetConfig "TAX_REDUCE_SELL_PERCENT" "$BSC_CONFIG_TYPE_OPTION"
TAX_REDUCE_SELL_PERCENT="$BSC_LAST_READ_CONFIG"

checkAndSetConfig "TAX_REDUCE_LIBERAL_PERCENT" "$BSC_CONFIG_TYPE_OPTION"
TAX_REDUCE_LIBERAL_PERCENT="$BSC_LAST_READ_CONFIG"

declare -g DEFAULT_YEAR
DEFAULT_YEAR=$( date "+%Y" )

SAVINGS_TAX_OPTION_UNDEFINED=-1
SAVINGS_TAX_OPTION_PROGRESSIVE=0
SAVINGS_TAX_OPTION_FLAT=1

#####################################################
#                Defines usages.
#####################################################
function usage {
  echo -e "usage: $0 -i|--income <income> [-c|--charges <charges>] [-p|--part <parts number>] [-S|--savings <savings> [--savingsTaxOption <flat | progressive> | --PFU | --2OP] [-r <bic income>] [-v <liberal income>] [-s <service income>] [-t <sell income>] [-R|--ri <rental income>] [-z] [--donation66 <donation> --donation75 <donation> --donationTips] [-y|--year <year of tax>] [-h|--help]"
  echo -e "-h|--help\t show this help"
  echo -e "<income>\t income to consider for the whole year"
  echo -e "<charges>\t potential charges to consider for the whole year"

  echo -e "<savings>\t Investment income (also known as taxable savings)"
  echo -e "<savingsTaxOpt.> Investment income tax option: choose 'flat' for the Flat Tax (PFU) or 'progressive' for the progressive scale (marginal rate!). "
  echo -e "--PFU\t\t Sets quickly the investment income tax option to 'flat'."
  echo -e "--2OP\t\t Sets quickly the investment income tax option to 'progressive'."

  echo -e "<parts number>\t number of parts, can be decimal (e.g. 2.5)"
  echo -e "<bic income>\t income of EURL/SARL in RSI (BIC IR)"
  echo -e "-z\t\t indicates CGA for BIC IR"
  echo -e "<liberal income> 'liberal profession' income"
  echo -e "<service income> 'société individuelle' for 'prestation de service' income"
  echo -e "<sell income>\t 'société individuelle' for 'ventes accessoires' income"
  echo -e "<rental income>\trental income ('location meublée' pour logement NON principal)"
  echo -e "<donation>\t\ttax-deductible donation amount to a non-profit organization (eligible for 66% or 75% tax reduction)"
  echo -e "--donationTips\t\tenable donation tips"
  echo -e "<year>\t\t year of tax (default: $DEFAULT_YEAR)"
  echo -e "\nExamples:"
  echo -e "Calculates income tax for 1 person, with 10000€ income, 1000€ charges:"
  echo -e "  $0 -i 10000 -c 1000"
  echo -e "\nCalculates income tax for 2 people, with total 20000€ income, and total 1000€ charges:"
  echo -e "  $0 -i 20000 -c 1000 -p 2"
  echo -e "\nNote concerning savings tax option:"
  echo -e "  By default, calculations are performed with the Flat Tax since 2018. Prior to 2018, the default was the progressive scale."
  echo -e "  Although the 2OP option did not exist before 2018, this script allows you to simulate old tax scenarios using the flat option."
}

# Usage: getSavingsTaxOptionFromOptionStr <option string>
function getSavingsTaxOptionFromOptionStr() {
  if [[ "$1" == "flat" ]]; then
    echo "$SAVINGS_TAX_OPTION_FLAT"
  elif [[ "$1" == "progressive" ]]; then
    echo "$SAVINGS_TAX_OPTION_PROGRESSIVE"
  else
    usage
    errorMessage "Unknown savings tax option '$1', should be either 'flat', or 'progressive'"
  fi
}

# Usage: getSavingsTaxOptionToOptionStr <option value>
function getSavingsTaxOptionToOptionStr() {
  if [[ "$1" == "$SAVINGS_TAX_OPTION_FLAT" ]]; then
    echo "flat"
  elif [[ "$1" == "$SAVINGS_TAX_OPTION_PROGRESSIVE" ]]; then
    echo "progressive"
  else
    usage
    errorMessage "Unknown savings tax option '$1', should be either '$SAVINGS_TAX_OPTION_FLAT', or '$SAVINGS_TAX_OPTION_PROGRESSIVE'"
  fi
}

#####################################################
#                Command line management.
#####################################################

hasCga=0
year=$DEFAULT_YEAR

if [ "${_DISABLE_FOR_TESTS:-0}" -ne 1 ]; then
  while [ "${1:-}" != "" ]; do
    if [ "$1" == "-i" ] || [ "$1" = "--income" ]; then
      shift
      income="$1"
    elif [ "$1" == "-r" ]; then
      shift
      incomeBIC="$1"
    elif [ "$1" == "-v" ]; then
      shift
      incomeLiberal="$1"
    elif [ "$1" == "-s" ]; then
      shift
      incomeService="$1"
    elif [ "$1" == "-t" ]; then
      shift
      incomeSell="$1"
    elif [ "$1" == "-z" ]; then
      hasCga=1
    elif [ "$1" == "-R" ] || [ "$1" == "--ri" ]; then
      shift
      incomeRental="$1"
    elif [ "$1" == "-c" ] || [ "$1" = "--charges" ]; then
      shift
      charges="$1"
    elif [ "$1" == "-p" ] || [ "$1" = "--part" ]; then
      shift
      partCount="$1"
    elif [ "$1" == "-y" ] || [ "$1" = "--year" ]; then
      shift
      year="$1"
    elif [ "$1" == "-S" ] || [ "$1" = "--savings" ]; then
      shift
      savings="$1"
    elif [ "$1" == "--savingsTaxOption" ]; then
      shift
      savingsTaxOption=$(  getSavingsTaxOptionFromOptionStr "$1" ) || exit 1
    elif [ "$1" == "--PFU" ]; then
      savingsTaxOption=$SAVINGS_TAX_OPTION_FLAT
    elif [ "$1" == "--2OP" ]; then
      savingsTaxOption=$SAVINGS_TAX_OPTION_PROGRESSIVE
    elif [ "$1" = "--donation66" ]; then
      shift
      donation66="$1"
    elif [ "$1" = "--donation75" ]; then
      shift
      donation75="$1"
    elif [ "$1" = "--donationTips" ]; then
      donationTips=1
    elif [ "$1" = "-h" ] || [ "$1" = "--help" ]; then
      usage
      exit 0
    else
      usage
      errorMessage "Unknown parameter '$1'"
    fi

    shift
  done

  [ -z "${income:-}" ] && usage && errorMessage "You must specify at least income"
fi


#####################################################
#                Functions
#####################################################

# usage: isDecimalGreaterThan <value1> <value2>
# Compares value1 to value2 and returns True if the first one is greater than the first one (any value can be decimal).
function isDecimalGreaterThan() {
  [ "$( echo "$1 > $2" |bc )" -eq 1 ]
}

# usage: isDecimalEqual <value1> <value2>
# Compares value1 to value2 and returns True if the first one is greater than the first one (any value can be decimal).
function isDecimalEqual() {
  [ "$( echo "$1 == $2" |bc )" -eq 1 ]
}

# usage: roundToInteger <decimal number> [<offset>]
# Rounds a decimal number to the nearest integer.
function roundToInteger() {
  local _value="$1" _offset="${2:-0.5}"
  echo "scale=0; ($_value + $_offset) / 1" |bc
}

# usage: calculate <math formula> [<0|1> retain_fraction]
# Evaluates a mathematical expression and rounds the result to the nearest integer
#  if the last argument is 0, or retains the fractional part if the last argument is 1.
function calculate() {
  local _formula="$1" _retainFraction="${2:-0}"

  result=$( echo "scale=2; $_formula" |bc )

  if [ "$_retainFraction" -eq 0 ]; then
    roundToInteger "$result"
  else
    echo "$result"
  fi
}

# usage: applyPercent <value> <percent> [<0|1> retain_fraction]
# Applies a percentage to a value. The result is rounded to the nearest integer
#  if the last argument is 0, or retains the fractional part if the last argument is 1.
function applyPercent() {
  local _value="$1" _percent="$2" _retainFraction="${3:-0}"
  calculate "$_value*$_percent/100" "$_retainFraction"
}

# usage: addOrRemoveAppliedPercent <value> <percent> [<multiplier>]
# Applies a percentage to a value, optionally multiplied by a given factor, and rounds the result to the nearest integer.
function addOrRemoveAppliedPercent() {
  local _value="$1" _percent="$2" _multiplier="${3:--1}"

  appliedPercent=$(  applyPercent "$_value" "$_percent" )
  echo $(( _value + _multiplier * appliedPercent ))
}

# usage: giveDonationTips <year config> <incomeRef> <income tax without donation> <income tax without donation percent>
function giveDonationTips() {
  # shellcheck disable=SC2178
  declare -n config="$1"
  local _incomeRef="$2" _incomeTaxWithoutDonation="$3" _incomeTaxWithoutDonationPercent="$4" _incomeTaxRateTarget _donation66IncomeTaxTarget

  echo
  writeMessage "To reduce your income tax rate, consider making a donation to a non-profit organization eligible for a 66% tax deduction (excluding any donations already specified during this execution or those eligible for a 75% deduction):"
  _incomeTaxRateTarget=$( roundToInteger "$_incomeTaxWithoutDonationPercent" "0" )
  while [ "$_incomeTaxRateTarget" -ge 0 ]; do
    _donation66IncomeTaxTarget=$( calculate "100*($_incomeTaxWithoutDonation-($_incomeTaxRateTarget*$_incomeRef/100))/66" )

    # Safe-guards:
    #  - ensures the donation amount is not greater than the tax without the donation
    #  - ensures the donation amount does not exceed the limit based on reference income
    isDecimalGreaterThan "$_donation66IncomeTaxTarget" "$_incomeTaxWithoutDonation" && break
    isDecimalGreaterThan "$_donation66IncomeTaxTarget" "${config['donation66Limit']}" && break

    writeMessage "    $_incomeTaxRateTarget% taxes, with a $_donation66IncomeTaxTarget€ donation"

    _incomeTaxRateTarget=$(( _incomeTaxRateTarget - 1 ))
  done
}

# checkDonation66AndComplete <year config> <incomeRef>
function checkDonation66AndComplete() {
  # shellcheck disable=SC2178
  declare -n config="$1"
  local _incomeRef="$2" _donation66Limit
  local _donation66="${config['donation66']}" _donation75="${config['donation75']}"

  local DONATION66_MAX_RATIO_OVER_REF=${config['DONATION66_MAX_RATIO_OVER_REF']}

  # Ensures that tax-deductible donations at 66% do not exceed the limit based on reference income.
  _donation66Limit=$( applyPercent "$_incomeRef" "$DONATION66_MAX_RATIO_OVER_REF" )
  if [ "$_donation66" -gt "$_donation66Limit" ]; then
      warning "Tax-deductible donations at 66% cannot exceed $DONATION66_MAX_RATIO_OVER_REF% of your reference income ($_incomeRef). Therefore, only $_donation66Limit (out of $_donation66) will be considered for this rate."
      _donation66=$_donation66Limit
  fi

  # Calculates and registers reduction related to donation amounts in the configuration.
  config+=(
    ["donation66"]=$_donation66 \
    ["donation66Limit"]=$_donation66Limit \
    ["donation66TaxReduction"]=$( applyPercent "$_donation66" "66" ) \
    ["donation75TaxReduction"]=$( applyPercent "$_donation75" "75" ) \
  )
}

# checkDonation75 <year config> <donation66> <donation75>
function checkDonation75() {
  # shellcheck disable=SC2178
  declare -n config="$1"
  local _donation66="$2" _donation75="$3" _donation75Remaining _areDonationAmountsUpdated

  local DONATION75_LIMIT=${config['DONATION75_LIMIT']}

  # Tax-deductible donations at 75% cannot exceed the specific year limit.
  _areDonationAmountsUpdated=0
  while [ "$_donation75" -gt "$DONATION75_LIMIT" ]; do
    _donation75Remaining=$(( _donation75 - DONATION75_LIMIT > DONATION75_LIMIT ? _donation75 - DONATION75_LIMIT : DONATION75_LIMIT ))

    # Donations exceeding the limit will be considered 66% tax-deductible.
    _donation66=$(( _donation66 + _donation75 - _donation75Remaining))
    _donation75=$_donation75Remaining
    _areDonationAmountsUpdated=1
  done

  # Updates the donation amounts in the configuration.
  config+=( ["donation66"]=$_donation66 ["donation75"]=$_donation75 )

  [ $_areDonationAmountsUpdated -eq 0 ] && return 0
  warning "Tax-deductible donations at 75% cannot exceed the limit of $DONATION75_LIMIT€; any amount exceeding this limit will automatically be considered as 66% tax-deductible."
}


# usage: read_year_config <year>
function read_year_config() {
  local _year="$1"

  specific_conf_file="$conf_files_dir/$_year.conf"
  [ ! -f "$specific_conf_file" ] && errorMessage "Specified year '$_year' is not supported"

  # N.B.: loads both global configuration file, and specific year configuration file which will override default values if needed.
  BSC_CONFIG_FILE="$specific_conf_file" loadConfigKeyValueList

  # Creates a global alias improving legibility.
  declare -gn year_config="BSC_LAST_READ_CONFIG_KEY_VALUE_LIST"

  # Computes some values.
  year_config+=(
    ["TAX_SOCIAL_TOTAL"]=$( calculate "${year_config['TAX_SOCIAL_CSG']}+${year_config['TAX_SOCIAL_CRDS']}+${year_config['TAX_SOCIAL_OTHERS']}" "1" )
    ["TAX_SOCIAL_CSG_CRDS"]=$( calculate "${year_config['TAX_SOCIAL_CSG']}+${year_config['TAX_SOCIAL_CRDS']}" "1" )
  )
}

# usage: calculateIncomeTax <year> <income> <charges> <part count> <bic income> <service income> <sell income> <liberal income> <rental income> <savings tax option> <taxable savings>
function calculateIncomeTax() {
  local _year="$1" _income="$2" _charges="$3" _partCount="$4"
  local _incomeBIC="$5" _incomeService="$6" _incomeSell="$7" _incomeLiberal="$8" _incomeRental="${9:-0}"
  local _savingsTaxOption="${10}" _savings="${11}"
  local _donation66="${12}" _donation75="${13}" _donationTips="${14}"

  # Reads configuration file of specified year.
  read_year_config "$_year"
  declare -a incomeTaxSteps=${year_config["INCOME_TAX_STEPS"]}
  declare -a incomeTaxStepsPercent=${year_config["INCOME_TAX_STEPS_PERCENT"]}
  local NO_CGA_MAJO_PERCENT=${year_config["NO_CGA_MAJO_PERCENT"]}
  local _savingsTaxRate=${year_config["TAX_SAVINGS_RATE"]}

  # Defines the savings tax option with specific year configuration, if not defined/overridden by caller.
  savingsTaxHeader=""
  if [[ $_savings -gt 0 ]]; then
    if [[ "$_savingsTaxOption" == "$SAVINGS_TAX_OPTION_UNDEFINED" ]]; then
      _savingsTaxOption=$(  getSavingsTaxOptionFromOptionStr "${year_config["TAX_SAVINGS_OPTION_DEFAULT"]}" ) || exit 1
      savingsTaxHeader=" (default savings tax option: ${year_config["TAX_SAVINGS_OPTION_DEFAULT"]})"
    else
      savingsTaxHeader=" (specified savings tax option: $( getSavingsTaxOptionToOptionStr "$_savingsTaxOption" ))"
    fi
  fi

  # Initial value.
  local income2Consider="$_income" _incomeRental2Consider _incomeRentalReduction _incomeRentalReductionText
  local _socialTaxCsgCrds _socialTaxPrelSol _socialTax _csgDeductible _pfu _donation66Limit

  # Informs.
  writeMessage "Calculating the French tax for your $((year-1)) income, applicable in the year $year$savingsTaxHeader."

  # Manages Tax-deductible donations at 75%.
  checkDonation75 year_config "$_donation66" "$_donation75"

  # Manages income reduction.
  income2Consider=$( addOrRemoveAppliedPercent "$income2Consider" "$TAX_REDUCE_INCOME_PERCENT" )
  writeMessage "  Income\t\t(reduction=$TAX_REDUCE_INCOME_PERCENT%)  $income2Consider ... "

  # Manages Rental income if any.
  if isDecimalGreaterThan "$_incomeRental" "0"; then
    # Calculates rental income reduction, taking care of minimal reduction.
    _incomeRentalReduction=$( addOrRemoveAppliedPercent "$_incomeRental" "${year_config['TAX_REDUCE_RENTAL_PERCENT']}" )
    _incomeRentalReductionText="${year_config['TAX_REDUCE_RENTAL_PERCENT']}%"

    if ! isDecimalGreaterThan "$_incomeRentalReduction" "${year_config['TAX_REDUCE_RENTAL_MIN']}"; then
      _incomeRentalReduction="${year_config['TAX_REDUCE_RENTAL_MIN']}"
      _incomeRentalReductionText="${year_config['TAX_REDUCE_RENTAL_MIN']}€"
    fi

    # Considers rental income if there are still some, after reduction.
    _incomeRental2Consider=$( calculate "$_incomeRental-$_incomeRentalReduction" )
    if isDecimalGreaterThan "$_incomeRental2Consider" "0"; then
      income2Consider=$( calculate "$income2Consider+$_incomeRental2Consider" )
      writeMessage "+ Rental Income\t(reduction=$_incomeRentalReductionText)  $income2Consider ... "
    fi
  fi

  # Manages BIC income if any.
  if isDecimalGreaterThan "$_incomeBIC" "0"; then
    if [ $hasCga -eq 1 ]; then
      income2Consider=$( calculate "$income2Consider+$_incomeBIC" )
      writeMessage "+ BIC Income\t\t(NO  reduction)  $income2Consider ... "
    else
      income2Consider=$(( income2Consider + $( addOrRemoveAppliedPercent "$_incomeBIC" "$NO_CGA_MAJO_PERCENT" "+1" ) ))
      writeMessage "+ BIC Income\t\t(majo     =$NO_CGA_MAJO_PERCENT%)  $income2Consider ... "
    fi
  fi

  # Manages Service income if any.
  if isDecimalGreaterThan "$_incomeService" "0"; then
    income2Consider=$(( income2Consider + $( addOrRemoveAppliedPercent "$_incomeService" "$TAX_REDUCE_SERVICE_PERCENT" ) ))
    writeMessage "+ Service Income\t(reduction=$TAX_REDUCE_SERVICE_PERCENT%)  $income2Consider ... "
  fi

  # Manages Sell income if any.
  if isDecimalGreaterThan "$_incomeSell" "0"; then
    income2Consider=$(( income2Consider + $( addOrRemoveAppliedPercent "$_incomeSell" "$TAX_REDUCE_SELL_PERCENT" ) ))
    writeMessage "+ Sell Income\t\t(reduction=$TAX_REDUCE_SELL_PERCENT%)  $income2Consider ... "
  fi

  # Manages Liberal income if any.
  if isDecimalGreaterThan "$_incomeLiberal" "0"; then
    income2Consider=$(( income2Consider + $( addOrRemoveAppliedPercent "$_incomeLiberal" "$TAX_REDUCE_LIBERAL_PERCENT" ) ))
    writeMessage "+ Liberal Income\t(reduction=$TAX_REDUCE_LIBERAL_PERCENT%)  $income2Consider ... "
  fi

  # Memorizes the reference income.
  incomeRef=$( calculate "$income2Consider+$_savings-$_charges" )

  # Manages taxable savings.
  if [[ $_savings -gt 0 && $_savingsTaxOption -eq $SAVINGS_TAX_OPTION_PROGRESSIVE ]]; then
    income2Consider=$( calculate "$income2Consider+$_savings" )
    writeMessage "+ savings \t\t($_savings)\t\t $income2Consider ... "

    csgDeductible=$( applyPercent "$_savings" "${year_config['TAX_SOCIAL_CSG_DEDUCTIBLE']}" )
    income2Consider=$( calculate "$income2Consider-$csgDeductible" )
    writeMessage "- CSG Deductible \t($csgDeductible)\t\t $income2Consider ... "

    # Updates the reference income.
    incomeRef=$( calculate "$incomeRef-$csgDeductible" )
  fi

  # Removes charges if any.
  if isDecimalGreaterThan "$_charges" "0"; then
    income2Consider=$( calculate "$income2Consider-$_charges" )
    writeMessage "- charges \t\t($_charges)\t\t $income2Consider ... "
  fi

  # Memorizes the taxable income.
  incomeTaxable=$( roundToInteger "$income2Consider" )
  echo
  writeMessage "  your income reference:\t\t $incomeRef"
  writeMessage "  your taxable income:\t\t $incomeTaxable"

  # Ensures that 66% tax-deductible donations do not exceed the limit based on reference income
  #  and calculates the corresponding tax reduction.
  checkDonation66AndComplete "year_config" "$incomeRef"

  # Takes care of part count.
  if isDecimalGreaterThan "$_partCount" "1"; then
    income2Consider=$( calculate "$income2Consider/$_partCount" )
    writeMessage "  divided by part count: $income2Consider ... "
  fi

  # For each step.
  incomeTax=0
  echo
  writeMessageSL "=>Income tax calculation for one part=0"
  lastStep=${incomeTaxSteps[1]}

  # Checks if income are greater than the "last" step, otherwise there is no tax.
  if ! isDecimalGreaterThan "$income2Consider" "$lastStep"; then
    echo ""
  else
    for step in {2..5}; do
      incomeTaxStep=${incomeTaxSteps[$step]}
      incomeTaxPercent=${incomeTaxStepsPercent[$step]}

      if isDecimalGreaterThan "$income2Consider" "$incomeTaxStep"; then
        incomePart=$( calculate "$incomeTaxStep-$lastStep" )
      else
        incomePart=$( calculate "$income2Consider-$lastStep" )
        income2Consider=0
      fi
      lastStep=$incomeTaxStep

      incomeTaxPart=$( applyPercent "$incomePart" "$incomeTaxPercent" "1" )
      echo -ne "+$incomeTaxPart"
      incomeTax=$( calculate "$incomeTax+$incomeTaxPart" "1" )

      isDecimalEqual "$income2Consider" "0" && break
    done

    # Rounds to integer the income tax.
    incomeTax=$( roundToInteger "$incomeTax" )

    # Informs about maximal tax step %.
    marginalTaxRate=$incomeTaxPercent
    echo -e " (Marginal tax rate: $marginalTaxRate%)"

    # Takes care of part count (must * by part count).
    if isDecimalGreaterThan "$_partCount" "1"; then
      writeMessageSL "  Tax for $_partCount parts=$incomeTax*$_partCount"
      incomeTax=$( calculate "$incomeTax*$_partCount" )
      echo "=$incomeTax"
    fi

    # Registers income Tax before Credits without donation.
    incomeTaxBeforeCreditsWithoutDonation=$incomeTax

    # Updates income tax with reductions from tax-deductible donations.
    if [ "${year_config['donation75TaxReduction']}" -gt 0 ]; then
      writeMessageSL "  Reduction due to 75% tax-deductible donations=$incomeTax-${year_config['donation75']}*75%"
      incomeTax=$( calculate "$incomeTax-${year_config['donation75TaxReduction']}" )
      echo -e "=$incomeTax"
    fi

    if [ "${year_config['donation66TaxReduction']}" -gt 0 ]; then
      writeMessageSL "  Reduction due to 66% tax-deductible donations=$incomeTax-${year_config['donation66']}*66%"
      incomeTax=$( calculate "$incomeTax-${year_config['donation66TaxReduction']}" )
      echo -e "=$incomeTax"
    fi

    # Registers income Tax before Credits.
    incomeTaxBeforeCredits="$incomeTax"

    # Manages already paid investment income tax.
    if [[ $_savings -gt 0 ]]; then
      echo
      writeMessage "=>Incorporating your Investment income ..."

      _pfu=$( applyPercent "$_savings" "$_savingsTaxRate" )

      if [[ $_savingsTaxOption -eq $SAVINGS_TAX_OPTION_PROGRESSIVE ]]; then
        writeMessage "- already paid tax (kept by your bank): $_pfu"
        incomeTax=$( calculate "$incomeTax-$_pfu" )
      else
        writeMessage "+ tax to pay ($_savingsTaxRate% on $_savings €)\t\t   $_pfu"
        # Note: PFU 'to pay' is added to income tax before credits.
        incomeTaxBeforeCredits=$( calculate "$incomeTax+$_pfu" "1" )
        incomeTaxBeforeCreditsWithoutDonation=$( calculate "$incomeTaxBeforeCreditsWithoutDonation+$_pfu" "1" )
        writeMessage "- already paid tax (kept by your bank)   $_pfu"
      fi
    fi

    # Calculates the income tax %.
    incomeTaxPercent=$( calculate "100*$incomeTaxBeforeCredits/$incomeRef" "1" )
    incomeTaxWithoutDonationPercent=$( calculate "100*$incomeTaxBeforeCreditsWithoutDonation/$incomeRef" "1" )

    # In cases, show the final tax value.
    echo
    writeMessageSL "Your income tax: $incomeTax -> rate=$incomeTaxPercent%"
    isDecimalEqual "$incomeTaxPercent" "$incomeTaxWithoutDonationPercent" && echo "" || echo " (rate=$incomeTaxWithoutDonationPercent% without donations)"

    # In cases there were considered rental income, social tax must be considered too.
    if isDecimalGreaterThan "${_incomeRental2Consider:-0}" "0"; then
      _socialTaxCsgCrds=$( applyPercent "$_incomeRental2Consider" "${year_config['TAX_SOCIAL_CSG_CRDS']}" )
      _socialTaxPrelSol=$( applyPercent "$_incomeRental2Consider" "${year_config['TAX_SOCIAL_OTHERS']}" )
      _socialTax=$(( _socialTaxCsgCrds + _socialTaxPrelSol ))
      echo
      writeMessage "Your social tax on rental income (considering $_incomeRental2Consider€) $_socialTax€ -> combined rate=${year_config['TAX_SOCIAL_TOTAL']}%"
      writeMessage "  Including CSG - CRDS (${year_config['TAX_SOCIAL_CSG_CRDS']}%):\t $_socialTaxCsgCrds€"
      writeMessage "  Including PREL SOL   (${year_config['TAX_SOCIAL_OTHERS']}%):\t $_socialTaxPrelSol€"
    fi

    # Gives donation tips if enabled.
    [ "$_donationTips" -eq 1 ] && giveDonationTips "year_config" "$incomeRef" "$incomeTaxBeforeCreditsWithoutDonation" "$incomeTaxWithoutDonationPercent"
  fi

  # Registers main values.
  major_income_tax_values["incomeRef"]=$incomeRef
  major_income_tax_values["incomeTax"]=$incomeTax
  major_income_tax_values["incomeTaxPercent"]=$incomeTaxPercent
  major_income_tax_values["incomeTaxWithoutDonationPercent"]=$incomeTaxWithoutDonationPercent
  major_income_tax_values["marginalTaxRate"]=$marginalTaxRate
  major_income_tax_values["incomeRental2Consider"]=${_incomeRental2Consider:-0}
  major_income_tax_values["socialTax"]=${_socialTax:-0}
  major_income_tax_values["TAX_SOCIAL_TOTAL"]=${year_config["TAX_SOCIAL_TOTAL"]}
}

#####################################################
#                Instructions
#####################################################

# Initialies associative global array to store important calculated information.
declare -gA major_income_tax_values=()
export major_income_tax_values

if [ "${_DISABLE_FOR_TESTS:-0}" -ne 1 ]; then
  # Requests income tax calculation.
  calculateIncomeTax "$year" "$income" "${charges:-0}" "${partCount:-1}" "${incomeBIC:-0}" "${incomeService:-0}" "${incomeSell:-0}" "${incomeLiberal:-0}" "${incomeRental:-0}" "${savingsTaxOption:-$SAVINGS_TAX_OPTION_UNDEFINED}" "${savings:-0}" "${donation66:-0}" "${donation75:-0}" "${donationTips:-0}"
fi
