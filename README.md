[![Codacy Badge](https://app.codacy.com/project/badge/Grade/f6127c2681d34b6cb0dc05c6a64fc70d)](https://app.codacy.com/gl/bertrand-benoit/incomeTaxCalculator/dashboard?utm_source=gl&utm_medium=referral&utm_content=&utm_campaign=Badge_grade)

# French income tax calculator
This is a free tool that allows you to calculate French income tax.


## Usage

To get complete usage:
```bash
  ./incomeTaxCalculator.sh --help
```

## Samples

### Sample 1 - One person
Calculates income tax for 1 person, with 10 000€ income, 1000€ charges:
```bash
  ./incomeTaxCalculator.sh -i 10000 -c 1000
```

### Sample 2 - Two people
Calculates income tax for 2 people, with total 20 000€ income, and total 1000€ charges:
```bash
  ./incomeTaxCalculator.sh -i 20000 -c 1000 -p 2
```

### Sample 3 - Two people and a child
Calculates income tax for 2 people, with total 20 000€ income, and total 1000€ charges:
```bash
  ./incomeTaxCalculator.sh -i 20000 -c 1000 -p 2.5

### Sample 3 - best month to be unit
You want to have an idea of the best month to be united?
```bash
  ./incomeTaxCalculator.sh -i 10000 -j 15000 -c 1000 -u 1-12
```

### Sample 4 - Just Married or PACSed (approximate calculation)
:warning: deprecated since version 2.0, if you want to use it, you can use the 1.5 version

-   calculates approximate income tax for 2 people united the 5th month
-   with 10 000€ as first income,
-   and 15 000€ as second income
-   and a total charges of 1000€
```bash
  ./incomeTaxCalculator.sh -i 10000 -j 15000 -c 1000 -u 5
```

### Sample 5 - Just Married or PACSed (precise calculation)
:warning: deprecated since version 2.0, if you want to use it, you can use the 1.5 version

Just Married or PACSed:
-   calculates precise income tax for 2 people united the xx/yy/zz, with precise income
-   first person: 3000€ earned from 01/01/zz to xx/yy/zz, and 7000€ earned from xx+1/12/zz
-   second person: 1000€ then 400€ for same periods
-   and precise charges, first person: 100€ then 400€ and second person: no charge
```bash
  ./incomeTaxCalculator.sh -i 3000 -k 7000 -j 1000 -l 400 -c 100 -d 400
```

## Contributing
Don't hesitate to [contribute](https://opensource.guide/how-to-contribute/) or to contact me if you want to improve the project.
You can [report issues or request features](https://gitlab.com/bertrand-benoit/incomeTaxCalculator/issues) and propose [merge requests](https://gitlab.com/bertrand-benoit/incomeTaxCalculator/merge_requests).

## Versioning
The versioning scheme we use is [SemVer](http://semver.org/).

## Authors
[Bertrand BENOIT](mailto:contact@bertrand-benoit.net)

## License
This project is under the GPLv3 License - see the [LICENSE](LICENSE) file for details
